control 'windows-licence-manager' do
    title "Ensure LicenceManager is enabled"
    desc "Test if LicenceManager is Enabled"
    describe service ('LicenceManager') do
        it {should be_installed}
        it {should be_enabled}
    end
end

