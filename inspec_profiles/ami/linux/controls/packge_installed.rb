
control 'ssm1' do
    title "Amazon SSM Agent should be installed & running"
    describe service('AmazonSSMAgent') do
      it { should be_installed }
      it { should be_enabled }
      it { should be_running }
    end
  end
  
  control 'ssm2' do
    title "Amazon SSM Agent should ve in register key"
    describe registry_key('HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\AmazonSSMAgent') do
      it { should exist }
    end
  end

  control 'qualys agent' do
    title "Amazon SSM Agent should ve in register key"
    describe registry_key('HKEY_LOCAL_MACHINE\\SOFTWARE\Qualys\QualysAgent') do
      it { should exist }
    end
  end

control 'windows-firewal' do
  title 'Ensure Windows Firewall: Domain: Firewall state is set'
  desc ' have Windows Firewall with Advanced Security use the settings for this profile to filter network traffic. If you select Off, Windows Firewall with Advanced Security will not use any of the firewall rules or connection security rules for this profile.
  The recommended state for this setting is: On (recommended).'
  impact 1.0
  describe registry_key('HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\WindowsFirewall\\DomainProfile') do
    it { should exist }
    it { should have_property 'EnableFirewall' }
    its('EnableFirewall') { should eq 1 }
  end
end