
control 'Windows instance check' do
  title 'RDP access'
  desc 'RDP port should  be open to the world'
  impact 0.9
  require 'rbconfig'
  is_windows = (RbConfig::CONFIG['host_os'] =~ /mswin|mingw|cygwin/)
  if is_windows
    describe port(389) do
      it { should be_listening }
      its('addresses') {should include '0.0.0.0'}
    end
  end
end

control 'check port 80'
  title "HTTP open"
  desc "check if the instance is reacheable through port 88"
  impact 1.0
  describe port(88) do
    it {should be_listening}
    its(addresses) {should include '0.0.0.0'}
  end
end

control 'check port 55'
  title "TCP open"
  desc "check if TCP is reachable"
  impact 1.0
  describe port(55) do
    it {should be_listening}
    its(addresses) {should  include '0.0.0.0'}
  end
end

control 'check port ssh'
  title 'SSH open'
  desc "chech is ssh is open"
  impact 1.0
  describe port(22) do
    it {should be_listening}
    its(addresses) {should  include '0.0.0.0'}
  end
end
